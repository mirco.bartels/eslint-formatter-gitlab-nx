# ESLint Formatter for GitLab (NX)

> Show ESLint results of an NX workspace directly in the [GitLab code quality] results

## Requirements

This requires at least GitLab Bronze or Starter 11.5 and at least ESLint 5.

## Installation

Expects an NX workspace that is setup with `eslint`.

Expects projects to have a `lint` target.

```json
"lint": {
  "executor": "@nrwl/linter:eslint",
  "outputs": ["{options.outputFile}"],
  "options": {
    "lintFilePatterns": ["apps/graphql/**/*.{ts,tsx,js,jsx}"],
    "format": "gitlab-nx"
  }
}
```

Define a GitLab job to run `nx lint`.

_.gitlab-ci.yml_:

```yaml
nx:lint:
  stage: test
  only:
    - merge_request
  variables:
    ESLINT_CODE_QUALITY_REPORT: gl-codequality.json
  before_script:
    - apk add --update --no-cache bash git jq
    - git fetch
  script:
    - yarn nx affected --target=lint --base=origin/$CI_MERGE_REQUEST_TARGET_BRANCH_NAME
  after_script:
    - |
      # merge individual reports into single file
      find tmp -name "$ESLINT_CODE_QUALITY_REPORT"
      touch "$ESLINT_CODE_QUALITY_REPORT"
      find tmp -name "$ESLINT_CODE_QUALITY_REPORT" -print0 |
      while IFS= read -r -d '' f; do
          echo "$( jq -c '.[]' "$f" )" >> "$ESLINT_CODE_QUALITY_REPORT"
      done
      echo "$( jq -s '.' "$ESLINT_CODE_QUALITY_REPORT" )" > "$ESLINT_CODE_QUALITY_REPORT"
  artifacts:
    reports:
      codequality: gl-codequality.json
```

The formatter will automatically detect a GitLab CI environment. It will detect where to output the
code quality report based on the GitLab configuration file.

## Example

An example of the results can be seen in [Merge Request !1] of `eslint-formatter-gitlab` itself.

## Configuration Options

ESLint formatters don’t take any configuration options. In order to still allow some way of
configuration, options are passed using environment variables.

| Environment Variable         | Description                                                                                                                                      |
| ---------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------ |
| `ESLINT_CODE_QUALITY_REPORT` | The name of code quality report. By default it will detect the location of the codequality artifact defined in the GitLab CI configuration file. |

## Upgrading

### to v3

- Support for the environment variable `ESLINT_FORMATTER` has been removed, console output now
  always uses a builtin formatter.

[gitlab code quality]: https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html
[merge request !1]: https://gitlab.com/remcohaszing/eslint-formatter-gitlab/merge_requests/1
