const { join } = require('path');

const { fs, vol } = require('memfs');

/**
 * @param {object[]} testEnv - A mapping of environment variables to use in the test.
 * @param {object[]} testWorkspace - The nx workspace definition.
 * @returns {Function} The patched module.
 */
function setup(testEnv, testWorkspace) {
  process.env = testEnv;
  jest.restoreAllMocks();
  jest.resetModules();
  jest.spyOn(process, 'cwd').mockReturnValue('/build');
  jest.setMock('fs', fs);
  vol.reset();
  vol.fromJSON({
    '/build/workspace.json': JSON.stringify(testWorkspace, null, 2),
  });
  // eslint-disable-next-line node/global-require
  return require('.');
}

it('should write a code quality report', () => {
  const formatter = setup(
    {
      CI_JOB_NAME: 'test-lint-job',
      NX_TASK_TARGET_PROJECT: 'api-graphql',
    },
    {
      projects: {
        'api-graphql': 'apps/api-graphql',
      },
    },
  );
  formatter([
    {
      filePath: join(process.cwd(), 'filename.js'),
      messages: [
        { line: 42, message: 'This is a linting error', ruleId: 'linting-error', severity: 2 },
        { line: 43, message: 'This is a linting warning', ruleId: 'linting-warning', severity: 1 },
      ],
    },
  ]);
  expect(
    JSON.parse(vol.readFileSync('/build/tmp/apps/api-graphql/gl-codequality.json')),
  ).toStrictEqual([
    {
      description: 'This is a linting error',
      fingerprint: '2235367ffc3cbeb18474a7285f9d5803',
      location: { lines: { begin: 42 }, path: 'filename.js' },
      severity: 'major',
    },
    {
      description: 'This is a linting warning',
      fingerprint: 'f86c96fb4ffdf46ae37fceed0d317715',
      location: { lines: { begin: 43 }, path: 'filename.js' },
      severity: 'minor',
    },
  ]);
});

it('should throw if no projects are defined', () => {
  const formatter = setup(
    {
      CI_JOB_NAME: 'test-lint-job',
      NX_TASK_TARGET_PROJECT: 'test',
    },
    {},
  );
  expect(() => formatter([])).toThrow(new Error("Expected 'projects' to be defined"));
});

it('should throw if NX_TASK_TARGET_PROJECT is not defined', () => {
  const formatter = setup(
    {
      CI_JOB_NAME: 'test-lint-job',
    },
    {},
  );
  expect(() => formatter([])).toThrow(new Error("Expected 'NX_TASK_TARGET_PROJECT' to be defined"));
});

it('should skip the output if CI_JOB_NAME is not defined', () => {
  const formatter = setup({}, {});
  formatter([]);
  expect(vol.existsSync('/build/tmp')).toBe(false);
});

it('should respect the ESLINT_CODE_QUALITY_REPORT environment variable', () => {
  const formatter = setup(
    {
      CI_JOB_NAME: 'test-lint-job',
      NX_TASK_TARGET_PROJECT: 'api-graphql',
      ESLINT_CODE_QUALITY_REPORT: 'something.json',
    },
    {
      projects: {
        'api-graphql': 'apps/api-graphql',
      },
    },
  );
  formatter([]);
  expect(JSON.parse(vol.readFileSync('/build/tmp/apps/api-graphql/something.json'))).toStrictEqual(
    [],
  );
});

it('should provide a report with URLs', () => {
  const formatter = setup(
    {
      CI_JOB_NAME: 'test-lint-job',
      CI_PROJECT_URL: 'https://gitlab.example.com/test/project',
      CI_COMMIT_SHORT_SHA: 'abc12345ef',
      NX_TASK_TARGET_PROJECT: 'api-graphql',
    },
    {
      projects: {
        'api-graphql': 'apps/api-graphql',
      },
    },
  );
  const result = formatter([
    {
      filePath: join(process.cwd(), 'filename.js'),
      errorCount: 1,
      warningCount: 0,
      messages: [
        {
          line: 42,
          column: 3,
          message: 'This is a linting error',
          ruleId: 'linting-error',
          severity: 2,
        },
        { message: 'This is a linting warning', ruleId: 'linting-warning', severity: 1 },
      ],
    },
  ]);

  expect(result).toBe(`
error  linting-error    This is a linting error    https://gitlab.example.com/test/project/-/blob/abc12345ef/filename.js#L42
warn   linting-warning  This is a linting warning  https://gitlab.example.com/test/project/-/blob/abc12345ef/filename.js

✖ 2 problems (1 error, 1 warning)
`);
});

it('should not try to create URLs when CI environment vars are not set', () => {
  const formatter = setup(
    {
      CI_JOB_NAME: 'test-lint-job',
      NX_TASK_TARGET_PROJECT: 'api-graphql',
    },
    {
      projects: {
        'api-graphql': 'apps/api-graphql',
      },
    },
  );
  const result = formatter([
    {
      filePath: join(process.cwd(), 'filename.js'),
      errorCount: 1,
      warningCount: 0,
      messages: [
        { line: 42, message: 'This is a linting error', ruleId: 'linting-error', severity: 2 },
      ],
    },
  ]);

  expect(result).toBe(`
error  linting-error  This is a linting error  /build/filename.js:42:0

✖ 1 problem (1 error, 0 warnings)
`);
});

it('should print a message when results are empty', () => {
  const formatter = setup(
    {
      CI_JOB_NAME: 'test-lint-job',
      NX_TASK_TARGET_PROJECT: 'api-graphql',
    },
    {
      projects: {
        'api-graphql': 'apps/api-graphql',
      },
    },
  );
  const result = formatter([]);

  expect(result).toBe(`
✔ No problems found
`);
});
