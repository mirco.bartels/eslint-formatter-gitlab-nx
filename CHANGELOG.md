# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project
adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.1] - 2022-02-19

### Fixed

- should write output if ESLINT_CODE_QUALITY_REPORT is set

## [1.0.0] - 2022-02-19

### Added

- Initial release.
- Forked from https://gitlab.com/remcohaszing/eslint-formatter-gitlab
- Adjusted to work with nx.dev workspace instead of relying on gitlab-ci config
